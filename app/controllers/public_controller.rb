class PublicController < ApplicationController
  

  def public_filter
    if !user_signed_in?
      redirect_to public_index_path
    end
  end
  def search_tag_get
    @tags = []
    @tags.push(Collection.find_by_name('label').content.split(',')).push(Collection.find_by_name('plang').content.split(',')).push(Collection.find_by_name('command').content.split(','))
    render :json => @tags  
  end
  def index
  	@article = Article.all.order(:updated_at).reverse_order
  end
  def weather
    @weather = sun(params[:country],params[:city],params[:lat],params[:lng])
    
        render :json => @weather
    
  end
  def view
    @article = Article.find(params[:id])
    cookies[:last_viewed] = params[:id]
    respond_to do |format|      
  	   format.html {@article}
       format.json {render :json => @article}
    end

  end
  
  def yadg
    
  end
  
  def search
  	ids = []
    articles = []    
  	labels = params[:search_label].split(',')
  	plangs = params[:search_plang].split(',')
  	commands = params[:search_command].split(',')
  	labels.empty? ? nil : ids.concat(search_add(Label,labels))
  	plangs.empty? ? nil : ids.concat(search_add(Plang,plangs))
  	commands.empty? ? nil : ids.concat(search_add(Command,commands))
    ids.each do |x|

      if articles.empty?        
        articles.concat(x)
      else
        articles = articles & x
      end
    end    
    @article = Article.find(articles)
     respond_to do |format|      
       format.html {@article}
       format.json {render :json => @article}
    end
  end
  private
  def sun(country,city,lat,lng)
    require 'open-uri'
    if weather = Weather.where(city: city,country: country).first
      time = Time.now
      if weather.updated_at < time.midnight       
        astronomy_get(weather,lat,lng)
        condition_get(weather,lat,lng)
        weather.save
      elsif weather.updated_at + 5.hour < time
        condition_get(weather,lat,lng)
        weather.save
      end
    else
      weather = Weather.new
      weather.city = city 
      weather.country = country
      astronomy_get(weather,lat,lng)
      condition_get(weather,lat,lng)
      weather.save
    end
    return weather
  end
  def astronomy_get(weather,lat,lng)
    url = URI.encode("http://api.wunderground.com/api/28f50fb6f4b2ea5f/astronomy/q/#{lat},#{lng}.json")
    open(url) do |x|
      data = x.read
      json = JSON.parse(data)
      weather.riseh = json['moon_phase']['sunrise']['hour']
      weather.risem = json['moon_phase']['sunrise']['minute']
      weather.seth = json['moon_phase']['sunset']['hour']
      weather.setm = json['moon_phase']['sunset']['minute']
      weather.moonphase = json['moon_phase']['ageOfMoon']
    end 
  end
  def condition_get(weather,lat,lng)
    url = URI.encode("http://api.wunderground.com/api/28f50fb6f4b2ea5f/conditions/q/#{lat},#{lng}.json")
    open(url) do |x|
      data = x.read
      json = JSON.parse(data)
      weather.condition =  json['current_observation']['weather']
      weather.temp =  json['current_observation']['temp_c']
      weather.wind =  json['current_observation']['wind_kph']
    end
  end
  def search_add(label_type,param)
    local_ids = []
    param.each do |x|
      local_ids.push(label_type.where(name: x).pluck(:article_id))
    end
    return local_ids   

  end
end
