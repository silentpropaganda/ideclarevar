class Admin::AdminController < Admin::BaseController
  # include Mercury::Authentication

  # layout :layout_with_mercury
  # helper_method :is_editing?

  def add
  	@article = Article.new
    @label = Label.uniq.pluck(:name)
    @plang = Plang.uniq.pluck(:name)
    @command = Command.uniq.pluck(:name)
    @body_class = 'add'
        
  end
  def save
  	@article = Article.new
  	@article.name = params[:article][:name]
  	@article.content = params[:article][:content]
  	@article.language = params[:article][:language]  
    if @article.save      
      property_save("labels",params[:label],false)    
      property_save("plangs",params[:plang],false)    
      property_save("commands",params[:command],false)    
      label_file_save
      redirect_to public_index_path
    else
      redirect_to :back
    end 	
  end
  def edit
    @article = Article.find(params[:id])    
    @article_label = @article.labels.pluck(:name)
    @article_plang = @article.plangs.pluck(:name)
    @article_command = @article.commands.pluck(:name)    
    @label = Label.uniq.pluck(:name) - @article_label
    @plang = Plang.uniq.pluck(:name) - @article_plang
    @command = Command.uniq.pluck(:name) - @article_command
    @body_class = 'add edit'
  end
  def destroy
    @article = Article.find(params[:id])
    @article.labels.destroy_all
    @article.plangs.destroy_all
    @article.commands.destroy_all
    @article.destroy
    label_file_save
    redirect_to '/'
  end

  def update
    @article = Article.find(params[:id])
    @article.update!(article_params)
    property_save("labels",params[:label],true)    
    property_save("plangs",params[:plang],true)    
    property_save("commands",params[:command],true)    
    label_file_save

    redirect_to '/'
  end
  private
  def property_save (property,param,update)    
    params = param.split(',')
    if update 
      existing_params = @article.send(property).all
      existing_params.each do |x|
        if !params.include?(x.name)
          params = params - [x.name] 
          x.destroy
        else
          params = params - [x.name] 
        end
      end
    end

    
    
    params.each do |x|
      s = @article.send(property).new
      s.name = x
      s.save      

    end
  end
  def label_file_save
    label = Collection.find_by_name('label')
    label.content = Label.uniq.pluck(:name).join(',')
    label.save
    plang = Collection.find_by_name('plang')
    plang.content = Plang.uniq.pluck(:name).join(',')
    plang.save
    command = Collection.find_by_name('command')
    command.content = Command.uniq.pluck(:name).join(',')
    command.save
     

  end

  def article_params
      params.require(:article).permit(:name, :content, :language)
  end


  
  # def layout_with_mercury
  #   !params[:mercury_frame] && is_editing? ? 'mercury' : 'application'
  # end

  # def is_editing?
  #   cookies[:editing] == 'true' && can_edit?
  # end
end
