class Admin::BaseController < ApplicationController
	before_filter :admin_filter

	def admin_filter
		if !user_signed_in?
			redirect_to index_path
		end
	end
end