class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :label_load
  def label_load
  	@label_file = JSON.parse(IO.read("public/label.json"))
  end
end
