class AddBaseStuff < ActiveRecord::Migration
  	def change
  		create_table :articles do |t|
	      	t.string :name
	  		  t.text :content
	      	t.integer :language          
	      	t.timestamps
    	end 
	    create_table :plangs do |t|
      		t.belongs_to :article
          t.string :name
		      t.timestamps
    	end
    	create_table :commands do |t|
        	t.belongs_to :article
          t.string :name        	
        	t.timestamps
  		end
	    
	    create_table :labels do |t|
        	t.belongs_to :article
          t.string :name  	
        	t.timestamps
    	end 
    end 
end
